package com.darkcube.bmgeditor.model;

public class Pointer {

	private int address;

	private int numRows;

	private String pointerText;

	private String choiceOneText;

	private String choiceTwoText;

	private boolean hasChoices = false;

	public Pointer(int address, int numRows) {
		this.address = address;
		this.numRows = numRows;
	}

	public int getAddress() {
		return address;
	}

	public void setAddress(int address) {
		this.address = address;
	}

	public void changeAddress(int difference) {
		address += difference;
	}

	public int getNumRows() {
		return numRows;
	}

	public void setNumRows(int numRows) {
		this.numRows = numRows;
	}

	public String getPointerText() {
		return pointerText;
	}

	public void setPointerText(String pointerText) {
		this.pointerText = pointerText;

		char[] firstChoiceTag = new char[3];
		firstChoiceTag[0] = (char) Integer.parseInt("001A", 16);
		firstChoiceTag[1] = (char) Integer.parseInt("0106", 16);
		firstChoiceTag[2] = (char) Integer.parseInt("0000", 16);

		String firstChoiceString = new String(firstChoiceTag);

		char[] secondChoiceTag = new char[3];
		secondChoiceTag[0] = (char) Integer.parseInt("001A", 16);
		secondChoiceTag[1] = (char) Integer.parseInt("0106", 16);
		secondChoiceTag[2] = (char) Integer.parseInt("0001", 16);

		String secondChoiceString = new String(secondChoiceTag);

		char[] endingChoiceTag = new char[3];
		endingChoiceTag[0] = (char) Integer.parseInt("001A", 16);
		endingChoiceTag[1] = (char) Integer.parseInt("0206", 16);
		endingChoiceTag[2] = (char) Integer.parseInt("0001", 16);

		String endingChoiceString = new String(endingChoiceTag);

		if (pointerText.contains(firstChoiceString) && pointerText.contains(secondChoiceString) && pointerText.contains(endingChoiceString)) {
			int firstChoiceStringLocation = pointerText.indexOf(firstChoiceString);
			int secondChoiceStringLocation = pointerText.indexOf(secondChoiceString);
			int endingChoiceStringLocation = pointerText.indexOf(endingChoiceString);
			choiceOneText = pointerText.substring(firstChoiceStringLocation + 3, secondChoiceStringLocation);
			choiceTwoText = pointerText.substring(secondChoiceStringLocation + 3, endingChoiceStringLocation);
			hasChoices = true;
		}
	}

	public String getChoiceOneText() {
		return choiceOneText;
	}

	public void setChoiceOneText(String choiceOneText) {
		this.choiceOneText = choiceOneText;
	}

	public String getChoiceTwoText() {
		return choiceTwoText;
	}

	public void setChoiceTwoText(String choiceTwoText) {
		this.choiceTwoText = choiceTwoText;
	}

	public boolean hasChoices() {
		return hasChoices;
	}

	public void setHasChoices(boolean hasChoices) {
		this.hasChoices = hasChoices;
	}

	public int length() {
		return pointerText.length();
	}

	public String getHeaderString() {
		char[] headerChars = new char[4];

		headerChars[0] = (char) address;
		headerChars[1] = (char) Integer.parseInt("0000", 16);
		headerChars[2] = (char) Integer.parseInt("0200", 16);
		headerChars[3] = (char) numRows;

		String headerString = new String(headerChars);

		return headerString;
	}
}
