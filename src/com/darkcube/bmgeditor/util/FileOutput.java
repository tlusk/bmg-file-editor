package com.darkcube.bmgeditor.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutput {

	public static void saveBytes(File file, byte[] bytes) throws IOException {
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
		out.write(bytes);
		out.close();
	}

}
