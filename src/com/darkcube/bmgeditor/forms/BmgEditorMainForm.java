package com.darkcube.bmgeditor.forms;

import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.darkcube.bmgeditor.actions.FileOpenAction;
import com.darkcube.bmgeditor.actions.FileSaveAction;
import com.darkcube.bmgeditor.actions.FileSaveAsAction;
import com.darkcube.bmgeditor.actions.PointerSaveAction;
import com.darkcube.bmgeditor.actions.PointerViewAction;
import com.darkcube.bmgeditor.model.BmgFile;
import com.darkcube.bmgeditor.model.Pointer;
import com.darkcube.bmgeditor.util.HexConverter;
import com.darkcube.bmgeditor.util.TextParser;

/**
 * 
 * @author __USER__
 */
public class BmgEditorMainForm extends javax.swing.JFrame implements ClipboardOwner {

	private BmgFile bmgFile;

	private TextParser parser = new TextParser();

	private int currentPointerDisplayed = -1;

	private boolean modified = false;

	private File openedFile;

	private static final long serialVersionUID = 1L;

	private Clipboard clipboard = getToolkit().getSystemClipboard();

	/** Creates new form HexApplication */
	public BmgEditorMainForm() {
		initComponents();
		GraphicsEnvironment.getLocalGraphicsEnvironment();
		Font font = new Font("Arial Unicode MS", Font.PLAIN, 12);
		mainTextArea.setFont(font);
		mainTextArea.setLineWrap(true);
		mainTextArea.setWrapStyleWord(true);
		choiceOneText.setFont(font);
		choiceTwoText.setFont(font);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SwingUtilities.updateComponentTreeUI(this);
		} catch (Exception e) {
		}
		setActions();
	}

	private void setActions() {
		miOpen.setAction(new FileOpenAction(this));
		miSave.setAction(new FileSaveAction(this));
		miSaveAs.setAction(new FileSaveAsAction(this));
		miSavePointer.setAction(new PointerSaveAction(this));
		miViewPointers.setAction(new PointerViewAction(this));
		miSave.setEnabled(false);
		miSaveAs.setEnabled(false);
		miSavePointer.setEnabled(false);
		miViewPointers.setEnabled(false);
	}

	public void setAllFields(String text, byte[] bytes) {
		modified = false;
		clearTextFields();

		char[] headerCode = new char[4];
		headerCode[0] = (char) Integer.parseInt("454D", 16);
		headerCode[1] = (char) Integer.parseInt("4753", 16);
		headerCode[2] = (char) Integer.parseInt("6D62", 16);
		headerCode[3] = (char) Integer.parseInt("3167", 16);

		String headerCodeString = new String(headerCode);
		String extractedHeaderCode = text.substring(0, 4);

		if (headerCodeString.equals(extractedHeaderCode)) {

			bmgFile = new BmgFile(text, bytes);

			bmgSizeText.setText(HexConverter.charToHex((char) bmgFile.getBmgSize()));
			inf1AddressText.setText(HexConverter.charToHex((char) bmgFile.getInf1Address()));
			inf1SizeText.setText(HexConverter.charToHex((char) bmgFile.getInf1Size()));
			dat1AddressText.setText(HexConverter.charToHex((char) bmgFile.getDat1Address()));
			dat1SizeText.setText(HexConverter.charToHex((char) bmgFile.getDat1Size()));
			flw1AddressText.setText(HexConverter.charToHex((char) bmgFile.getFlw1Address()));
			flw1SizeText.setText(HexConverter.charToHex((char) bmgFile.getFlw1Size()));
			fli1AddressText.setText(HexConverter.charToHex((char) bmgFile.getFli1Address()));
			fli1SizeText.setText(HexConverter.charToHex((char) bmgFile.getFli1Size()));
			numPointersText.setText("" + bmgFile.getNumPointers());

			setPointerSelected(0);

			miSave.setEnabled(true);
			miSaveAs.setEnabled(true);
			miSavePointer.setEnabled(true);
			miViewPointers.setEnabled(true);
			savePointerButton.setEnabled(true);
			nextPointerButton.setEnabled(true);

		} else {
			JOptionPane.showMessageDialog(this, "File is not a valid BMG file.", "Invalid File", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private void clearTextFields() {
		bmgSizeText.setText("");
		inf1AddressText.setText("");
		inf1SizeText.setText("");
		dat1AddressText.setText("");
		dat1SizeText.setText("");
		flw1AddressText.setText("");
		flw1SizeText.setText("");
		fli1AddressText.setText("");
		fli1SizeText.setText("");
		numPointersText.setText("");
		mainTextArea.setText("");
		pointerNumberLabel.setText("0");
		pointerLinesLabel.setText("0");
		clearChoiceFields();
	}

	private void clearChoiceFields() {
		choiceOneText.setText("");
		choiceTwoText.setText("");
	}

	public String getFileText() {
		return mainTextArea.getText();
	}

	public void setPointerSelected(int num) {
		Pointer pointer = bmgFile.getPointer(num);
		String pointerText = pointer.getPointerText();

		pointerText = parser.parseOutAllCodes(pointerText);

		mainTextArea.setText(pointerText);

		if (pointer.hasChoices()) {
			choiceOneText.setText(pointer.getChoiceOneText());
			choiceTwoText.setText(pointer.getChoiceTwoText());
		} else {
			clearChoiceFields();
		}

		currentPointerDisplayed = num;
		pointerNumberLabel.setText("" + (num + 1));
		pointerLinesLabel.setText("" + pointer.getNumRows());

		if (currentPointerDisplayed <= 0) {
			previousPointerButton.setEnabled(false);
			nextPointerButton.setEnabled(true);
		} else if (currentPointerDisplayed >= bmgFile.getNumPointers() - 1) {
			previousPointerButton.setEnabled(true);
			nextPointerButton.setEnabled(false);
		} else {
			previousPointerButton.setEnabled(true);
			nextPointerButton.setEnabled(true);
		}

		mainTextAreaKeyTyped(null);
	}

	public void dispose() {
		if (!modified) {
			System.exit(0);
		} else {
			int retVal = JOptionPane.showConfirmDialog(this, "Text in " + openedFile.getName() + " has changed.\n\nDo you want to save the changes?", "Zelda BMG Editor",
					JOptionPane.YES_NO_CANCEL_OPTION);
			if (retVal == JOptionPane.CANCEL_OPTION) {
				return;
			} else if (retVal == JOptionPane.NO_OPTION) {
				System.exit(0);
			} else if (retVal == JOptionPane.YES_OPTION) {
				FileSaveAction save = new FileSaveAction(this);
				save.actionPerformed(null);
				System.exit(0);
			}
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc=" Generated Code ">
	private void initComponents() {
		jScrollPane1 = new javax.swing.JScrollPane();
		mainTextArea = new javax.swing.JTextArea();
		jPanel1 = new javax.swing.JPanel();
		inf1AddressText = new javax.swing.JTextField();
		jLabel1 = new javax.swing.JLabel();
		dat1AddressText = new javax.swing.JTextField();
		jLabel5 = new javax.swing.JLabel();
		flw1AddressText = new javax.swing.JTextField();
		jLabel6 = new javax.swing.JLabel();
		fli1AddressText = new javax.swing.JTextField();
		jLabel7 = new javax.swing.JLabel();
		jPanel2 = new javax.swing.JPanel();
		inf1SizeText = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		dat1SizeText = new javax.swing.JTextField();
		jLabel12 = new javax.swing.JLabel();
		flw1SizeText = new javax.swing.JTextField();
		jLabel13 = new javax.swing.JLabel();
		fli1SizeText = new javax.swing.JTextField();
		jLabel14 = new javax.swing.JLabel();
		jPanel3 = new javax.swing.JPanel();
		numPointersText = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		bmgSizeText = new javax.swing.JTextField();
		jLabel4 = new javax.swing.JLabel();
		previousPointerButton = new javax.swing.JButton();
		nextPointerButton = new javax.swing.JButton();
		jLabel8 = new javax.swing.JLabel();
		pointerNumberLabel = new javax.swing.JLabel();
		savePointerButton = new javax.swing.JButton();
		choiceOneText = new javax.swing.JTextField();
		jLabel9 = new javax.swing.JLabel();
		jLabel10 = new javax.swing.JLabel();
		choiceTwoText = new javax.swing.JTextField();
		jLabel11 = new javax.swing.JLabel();
		pointerLinesLabel = new javax.swing.JLabel();
		lazyPasteButton = new javax.swing.JButton();
		jMenuBar1 = new javax.swing.JMenuBar();
		jMenu1 = new javax.swing.JMenu();
		miOpen = new javax.swing.JMenuItem();
		miSave = new javax.swing.JMenuItem();
		miSaveAs = new javax.swing.JMenuItem();
		jSeparator1 = new javax.swing.JSeparator();
		miExit = new javax.swing.JMenuItem();
		jMenu2 = new javax.swing.JMenu();
		miViewPointers = new javax.swing.JMenuItem();
		miSavePointer = new javax.swing.JMenuItem();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Zelda BMG Editor");
		setName("hexDialog");
		setResizable(false);
		mainTextArea.setColumns(20);
		mainTextArea.setRows(5);
		mainTextArea.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				mainTextAreaKeyTyped(evt);
			}
		});

		jScrollPane1.setViewportView(mainTextArea);

		jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Block Addresses"));
		inf1AddressText.setEditable(false);

		jLabel1.setText("INF1 Address:");

		dat1AddressText.setEditable(false);

		jLabel5.setText("DAT1 Address:");

		flw1AddressText.setEditable(false);

		jLabel6.setText("FLW1 Address:");

		fli1AddressText.setEditable(false);

		jLabel7.setText("FLI1 Address:");

		org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				jPanel1Layout.createSequentialGroup().addContainerGap().add(
						jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING).add(jLabel1).add(jLabel5).add(jLabel6).add(jLabel7)).addPreferredGap(
						org.jdesktop.layout.LayoutStyle.RELATED).add(
						jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, dat1AddressText,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE).add(org.jdesktop.layout.GroupLayout.TRAILING, fli1AddressText,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE).add(org.jdesktop.layout.GroupLayout.TRAILING, flw1AddressText,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE).add(
								jPanel1Layout.createSequentialGroup().add(inf1AddressText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE).addPreferredGap(
										org.jdesktop.layout.LayoutStyle.RELATED))).addContainerGap()));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				jPanel1Layout.createSequentialGroup().add(
						jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(jLabel1).add(inf1AddressText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(
						jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(dat1AddressText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(jLabel5)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
						.add(
								jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(flw1AddressText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(jLabel6)).addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED).add(
								jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(fli1AddressText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(jLabel7)).addContainerGap(
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Block Sizes"));
		inf1SizeText.setEditable(false);

		jLabel2.setText("INF1 Size:");

		dat1SizeText.setEditable(false);

		jLabel12.setText("DAT1 Size:");

		flw1SizeText.setEditable(false);

		jLabel13.setText("FLW1 Size:");

		fli1SizeText.setEditable(false);

		jLabel14.setText("FLI1 Size:");

		org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				jPanel2Layout.createSequentialGroup().add(30, 30, 30).add(
						jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING).add(jLabel13).add(jLabel14).add(jLabel12).add(jLabel2)).addPreferredGap(
						org.jdesktop.layout.LayoutStyle.RELATED).add(
						jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false).add(fli1SizeText).add(flw1SizeText).add(dat1SizeText).add(
								jPanel2Layout.createSequentialGroup().add(inf1SizeText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE).addPreferredGap(
										org.jdesktop.layout.LayoutStyle.RELATED))).addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				jPanel2Layout.createSequentialGroup().add(
						jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(inf1SizeText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(jLabel2)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
						.add(
								jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(dat1SizeText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(jLabel12)).addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED).add(
								jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(flw1SizeText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(jLabel13)).addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED).add(
								jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(fli1SizeText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(jLabel14)).addContainerGap(
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Other Information"));
		numPointersText.setEditable(false);

		jLabel3.setText("Num Pointers:");

		bmgSizeText.setEditable(false);

		jLabel4.setText("BMG Size:");

		org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				jPanel3Layout.createSequentialGroup().add(16, 16, 16).add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING).add(jLabel3).add(jLabel4)).addPreferredGap(
						org.jdesktop.layout.LayoutStyle.RELATED).add(
						jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(bmgSizeText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE).add(
								numPointersText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)).addContainerGap()));
		jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				jPanel3Layout.createSequentialGroup().add(
						jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(jLabel3).add(numPointersText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(
						jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(jLabel4).add(bmgSizeText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addContainerGap(16, Short.MAX_VALUE)));

		previousPointerButton.setText("Previous Pointer");
		previousPointerButton.setEnabled(false);
		previousPointerButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				previousPointerButtonActionPerformed(evt);
			}
		});

		nextPointerButton.setText("Next Pointer");
		nextPointerButton.setEnabled(false);
		nextPointerButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				nextPointerButtonActionPerformed(evt);
			}
		});

		jLabel8.setText("Displaying Pointer: ");

		pointerNumberLabel.setText("0");

		savePointerButton.setText("Save");
		savePointerButton.setEnabled(false);
		savePointerButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				savePointerButtonActionPerformed(evt);
			}
		});

		choiceOneText.setEditable(false);

		jLabel9.setText("Choice 1:");

		jLabel10.setText("Choice 2:");

		choiceTwoText.setEditable(false);

		jLabel11.setText("Pointer Lines:");

		pointerLinesLabel.setText("0");

		lazyPasteButton.setText("Lazy Paste");
		lazyPasteButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				lazyPasteButtonActionPerformed(evt);
			}
		});

		jMenu1.setText("File");
		miOpen.setText("Open");
		miOpen.setName("Open");
		jMenu1.add(miOpen);

		miSave.setText("Save");
		jMenu1.add(miSave);

		miSaveAs.setText("Item");
		miSaveAs.setEnabled(false);
		jMenu1.add(miSaveAs);

		jMenu1.add(jSeparator1);

		miExit.setText("Exit");
		miExit.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				miExitActionPerformed(evt);
			}
		});

		jMenu1.add(miExit);

		jMenuBar1.add(jMenu1);

		jMenu2.setText("Pointers");
		miViewPointers.setText("View Pointers...");
		miViewPointers.setEnabled(false);
		jMenu2.add(miViewPointers);

		miSavePointer.setText("Save Pointer...");
		miSavePointer.setEnabled(false);
		jMenu2.add(miSavePointer);

		jMenuBar1.add(jMenu2);

		setJMenuBar(jMenuBar1);

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				layout.createSequentialGroup().addContainerGap().add(
						layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
								layout.createSequentialGroup().add(
										layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 307, Short.MAX_VALUE).add(
												org.jdesktop.layout.GroupLayout.TRAILING,
												layout.createSequentialGroup().add(
														layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
																layout.createSequentialGroup().add(jLabel9).add(2, 2, 2).add(choiceOneText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 94,
																		org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 22, Short.MAX_VALUE)
																		.add(jLabel10).add(4, 4, 4)).add(
																layout.createSequentialGroup().add(previousPointerButton).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(
																		layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(lazyPasteButton).add(savePointerButton,
																				org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)).addPreferredGap(
																		org.jdesktop.layout.LayoutStyle.RELATED))).add(
														layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false).add(choiceTwoText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 95,
																Short.MAX_VALUE).add(nextPointerButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)))).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)).add(
								layout.createSequentialGroup().add(jLabel8).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(pointerNumberLabel).addPreferredGap(
										org.jdesktop.layout.LayoutStyle.RELATED, 128, Short.MAX_VALUE).add(jLabel11).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(pointerLinesLabel)
										.add(4, 4, 4))).add(
						layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false).add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				layout.createSequentialGroup().addContainerGap().add(
						layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
								layout.createSequentialGroup().add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(jPanel2,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).add(
								layout.createSequentialGroup().add(8, 8, 8).add(
										layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(jLabel8).add(pointerNumberLabel).add(pointerLinesLabel).add(jLabel11))
										.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 248,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(
						layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false).add(
								layout.createSequentialGroup().add(
										layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(jLabel9).add(jLabel10).add(choiceTwoText,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(
												choiceOneText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(lazyPasteButton).add(6, 6, 6).add(
										layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(previousPointerButton).add(savePointerButton).add(nextPointerButton))).add(jPanel3,
								org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).addContainerGap(23,
						Short.MAX_VALUE)));
		pack();
	}// </editor-fold>//GEN-END:initComponents

	//GEN-FIRST:event_lazyPasteButtonActionPerformed
	private void lazyPasteButtonActionPerformed(java.awt.event.ActionEvent evt) {
		Transferable content = clipboard.getContents(this);
		if (content != null) {
			try {
				String dstData = (String) content.getTransferData(DataFlavor.stringFlavor);
				for (int i = 21; i < dstData.length(); i += 22) {
					String part1 = dstData.substring(0, i);
					String part2 = dstData.substring(i, dstData.length());
					dstData = part1 + '\n' + part2;
				}
				mainTextArea.insert(dstData, mainTextArea.getCaretPosition());
			} catch (Exception e) {
				System.out.println("Couldn't get contents in format: " + DataFlavor.stringFlavor.getHumanPresentableName());
			}
		}
	}//GEN-LAST:event_lazyPasteButtonActionPerformed

	// GEN-FIRST:event_mainTextAreaKeyTyped
	private void mainTextAreaKeyTyped(java.awt.event.KeyEvent evt) {
		String allText = mainTextArea.getText();
		StringTokenizer st = new StringTokenizer(allText, "\n");
		boolean tooLong = false;
		while (st.hasMoreTokens()) {
			String line = st.nextToken();
			line = parser.parseAwayAllCodes(line);
			int length = line.length();
			if (!st.hasMoreTokens()) {
				length--;
			}
			if (length > 24) {
				tooLong = true;
				System.out.println("Line of size " + length + " too long : " + line);
			}
		}
		if (tooLong) {
			Color errorColor = Color.RED;
			mainTextArea.setForeground(errorColor);
			System.out.println('\n');
		} else {
			Color normalColor = Color.BLACK;
			mainTextArea.setForeground(normalColor);
		}
	}// GEN-LAST:event_mainTextAreaKeyTyped

	// GEN-FIRST:event_savePointerButtonActionPerformed
	private void savePointerButtonActionPerformed(java.awt.event.ActionEvent evt) {
		String pointerText = mainTextArea.getText();
		pointerText = parser.parseInAllCodes(pointerText);
		bmgFile.setPointerText(currentPointerDisplayed, pointerText);
		if (!modified) {
			modified = true;
			setTitle("Zelda BMG Editor *" + openedFile.getName());
		}
	}// GEN-LAST:event_savePointerButtonActionPerformed

	// GEN-FIRST:event_previousPointerButtonActionPerformed
	private void previousPointerButtonActionPerformed(java.awt.event.ActionEvent evt) {
		setPointerSelected(currentPointerDisplayed - 1);
	}// GEN-LAST:event_previousPointerButtonActionPerformed

	// GEN-FIRST:event_nextPointerButtonActionPerformed
	private void nextPointerButtonActionPerformed(java.awt.event.ActionEvent evt) {
		setPointerSelected(currentPointerDisplayed + 1);
	}// GEN-LAST:event_nextPointerButtonActionPerformed

	// GEN-FIRST:event_miExitActionPerformed
	private void miExitActionPerformed(java.awt.event.ActionEvent evt) {
		dispose();
	}// GEN-LAST:event_miExitActionPerformed

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JTextField bmgSizeText;

	private javax.swing.JTextField choiceOneText;

	private javax.swing.JTextField choiceTwoText;

	private javax.swing.JTextField dat1AddressText;

	private javax.swing.JTextField dat1SizeText;

	private javax.swing.JTextField fli1AddressText;

	private javax.swing.JTextField fli1SizeText;

	private javax.swing.JTextField flw1AddressText;

	private javax.swing.JTextField flw1SizeText;

	private javax.swing.JTextField inf1AddressText;

	private javax.swing.JTextField inf1SizeText;

	private javax.swing.JLabel jLabel1;

	private javax.swing.JLabel jLabel10;

	private javax.swing.JLabel jLabel11;

	private javax.swing.JLabel jLabel12;

	private javax.swing.JLabel jLabel13;

	private javax.swing.JLabel jLabel14;

	private javax.swing.JLabel jLabel2;

	private javax.swing.JLabel jLabel3;

	private javax.swing.JLabel jLabel4;

	private javax.swing.JLabel jLabel5;

	private javax.swing.JLabel jLabel6;

	private javax.swing.JLabel jLabel7;

	private javax.swing.JLabel jLabel8;

	private javax.swing.JLabel jLabel9;

	private javax.swing.JMenu jMenu1;

	private javax.swing.JMenu jMenu2;

	private javax.swing.JMenuBar jMenuBar1;

	private javax.swing.JPanel jPanel1;

	private javax.swing.JPanel jPanel2;

	private javax.swing.JPanel jPanel3;

	private javax.swing.JScrollPane jScrollPane1;

	private javax.swing.JSeparator jSeparator1;

	private javax.swing.JButton lazyPasteButton;

	private javax.swing.JTextArea mainTextArea;

	private javax.swing.JMenuItem miExit;

	private javax.swing.JMenuItem miOpen;

	private javax.swing.JMenuItem miSave;

	private javax.swing.JMenuItem miSaveAs;

	private javax.swing.JMenuItem miSavePointer;

	private javax.swing.JMenuItem miViewPointers;

	private javax.swing.JButton nextPointerButton;

	private javax.swing.JTextField numPointersText;

	private javax.swing.JLabel pointerLinesLabel;

	private javax.swing.JLabel pointerNumberLabel;

	private javax.swing.JButton previousPointerButton;

	private javax.swing.JButton savePointerButton;

	// End of variables declaration//GEN-END:variables

	public int getCurrentPointerDisplayed() {
		return currentPointerDisplayed;
	}

	public BmgFile getBmgFile() {
		return bmgFile;
	}

	public void setBmgFile(BmgFile bmgFile) {
		this.bmgFile = bmgFile;
	}

	public File getOpenedFile() {
		return openedFile;
	}

	public void setOpenedFile(File openedFile) {
		this.openedFile = openedFile;
	}

	public void lostOwnership(Clipboard arg0, Transferable arg1) {
		System.out.println("Clipboard contents replaced");
	}

}