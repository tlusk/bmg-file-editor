package com.darkcube.bmgeditor.util;

public class TextParser {

	private static final String BLACK_COLOR_CODE = "$BLACK_COLOR$";

	private String blackColorHexCode;

	private static final String RED_COLOR_CODE = "$RED_COLOR$";

	private String redColorHexCode;

	private static final String GREEN_COLOR_CODE = "$GREEN_COLOR$";

	private String greenColorHexCode;

	private static final String BLUE_COLOR_CODE = "$BLUE_COLOR$";

	private String blueColorHexCode;

	private static final String NAME_CODE = "$NAME$";

	private String nameHexCode;

	private static final String FILE_SLOT_CODE = "$FILE_SLOT$";

	private String fileSlotHexCode;

	private static final String VERY_SLOW_CODE = "$VERY_SLOW$";

	private String verySlowHexCode;

	private static final String SLOW_CODE = "$SLOW$";

	private String slowHexCode;
	
	private static final String PAUSE_CODE = "$PAUSE$";

	private String pauseHexCode;

	private static final String FAST_CODE = "$FAST$";

	private String fastHexCode;

	public TextParser() {
		char[] verySlowCodeChars = new char[4];
		verySlowCodeChars[0] = (char) Integer.parseInt("001A", 16);
		verySlowCodeChars[1] = (char) Integer.parseInt("0108", 16);
		verySlowCodeChars[2] = (char) Integer.parseInt("000A", 16);
		verySlowCodeChars[3] = (char) Integer.parseInt("001E", 16);

		verySlowHexCode = new String(verySlowCodeChars);

		char[] slowCodeChars = new char[4];
		slowCodeChars[0] = (char) Integer.parseInt("001A", 16);
		slowCodeChars[1] = (char) Integer.parseInt("0108", 16);
		slowCodeChars[2] = (char) Integer.parseInt("000A", 16);
		slowCodeChars[3] = (char) Integer.parseInt("0003", 16);

		slowHexCode = new String(slowCodeChars);
		
		char[] pauseCodeChars = new char[4];
		pauseCodeChars[0] = (char) Integer.parseInt("001A", 16);
		pauseCodeChars[1] = (char) Integer.parseInt("0108", 16);
		pauseCodeChars[2] = (char) Integer.parseInt("000A", 16);
		pauseCodeChars[3] = (char) Integer.parseInt("000F", 16);

		pauseHexCode = new String(pauseCodeChars);

		char[] fastCodeChars = new char[4];
		fastCodeChars[0] = (char) Integer.parseInt("001A", 16);
		fastCodeChars[1] = (char) Integer.parseInt("0108", 16);
		fastCodeChars[2] = (char) Integer.parseInt("0014", 16);
		fastCodeChars[3] = (char) Integer.parseInt("0000", 16);

		fastHexCode = new String(fastCodeChars);

		char[] fileSlotCodeChars = new char[4];
		fileSlotCodeChars[0] = (char) Integer.parseInt("001A", 16);
		fileSlotCodeChars[1] = (char) Integer.parseInt("FE08", 16);
		fileSlotCodeChars[2] = (char) Integer.parseInt("0001", 16);
		fileSlotCodeChars[3] = (char) Integer.parseInt("0000", 16);

		fileSlotHexCode = new String(fileSlotCodeChars);

		char[] nameCodeChars = new char[3];
		nameCodeChars[0] = (char) Integer.parseInt("001A", 16);
		nameCodeChars[1] = (char) Integer.parseInt("FE06", 16);
		nameCodeChars[2] = (char) Integer.parseInt("0000", 16);

		nameHexCode = new String(nameCodeChars);

		char[] blackCharColorCode = new char[4];
		blackCharColorCode[0] = (char) Integer.parseInt("001A", 16);
		blackCharColorCode[1] = (char) Integer.parseInt("FF08", 16);
		blackCharColorCode[2] = (char) Integer.parseInt("0000", 16);
		blackCharColorCode[3] = (char) Integer.parseInt("0000", 16);

		blackColorHexCode = new String(blackCharColorCode);

		char[] redCharColorCode = new char[4];
		redCharColorCode[0] = (char) Integer.parseInt("001A", 16);
		redCharColorCode[1] = (char) Integer.parseInt("FF08", 16);
		redCharColorCode[2] = (char) Integer.parseInt("0000", 16);
		redCharColorCode[3] = (char) Integer.parseInt("0001", 16);

		redColorHexCode = new String(redCharColorCode);

		char[] greenCharColorCode = new char[4];
		greenCharColorCode[0] = (char) Integer.parseInt("001A", 16);
		greenCharColorCode[1] = (char) Integer.parseInt("FF08", 16);
		greenCharColorCode[2] = (char) Integer.parseInt("0000", 16);
		greenCharColorCode[3] = (char) Integer.parseInt("0002", 16);

		greenColorHexCode = new String(greenCharColorCode);

		char[] blueCharColorCode = new char[4];
		blueCharColorCode[0] = (char) Integer.parseInt("001A", 16);
		blueCharColorCode[1] = (char) Integer.parseInt("FF08", 16);
		blueCharColorCode[2] = (char) Integer.parseInt("0000", 16);
		blueCharColorCode[3] = (char) Integer.parseInt("0003", 16);

		blueColorHexCode = new String(blueCharColorCode);
	}

	public String parseOutAllCodes(String inputString) {
		String retVal = inputString;
		try{
			retVal = parseOutHoverCodes(inputString);
		} catch( Exception e ) {
			e.printStackTrace();
		}
		retVal = parseOutBlackColorCode(retVal);
		retVal = parseOutRedColorCode(retVal);
		retVal = parseOutGreenColorCode(retVal);
		retVal = parseOutBlueColorCode(retVal);
		retVal = parseOutVerySlowCode(retVal);
		retVal = parseOutSlowCode(retVal);
		retVal = parseOutPauseCode(retVal);
		retVal = parseOutFastCode(retVal);
		retVal = parseOutNameCode(retVal);
		retVal = parseOutFileSlotNumberCode(retVal);
		return retVal;
	}

	public String parseInAllCodes(String inputString) {
		String retVal = parseInBlackColorCode(inputString);
		retVal = parseInRedColorCode(retVal);
		retVal = parseInGreenColorCode(retVal);
		retVal = parseInBlueColorCode(retVal);
		retVal = parseInVerySlowCode(retVal);
		retVal = parseInSlowCode(retVal);
		retVal = parseInPauseCode(retVal);
		retVal = parseInFastCode(retVal);
		retVal = parseInNameCode(retVal);
		retVal = parseInFileSlotNumberCode(retVal);
		return retVal;
	}
	
	public String parseAwayAllCodes(String inputString) {
		String retVal = parseAwayBlackColorCode(inputString);
		retVal = parseAwayRedColorCode(retVal);
		retVal = parseAwayGreenColorCode(retVal);
		retVal = parseAwayBlueColorCode(retVal);
		retVal = parseAwayVerySlowCode(retVal);
		retVal = parseAwaySlowCode(retVal);
		retVal = parseAwayPauseCode(retVal);
		retVal = parseAwayFastCode(retVal);
		retVal = parseAwayNameCode(retVal);
		retVal = parseAwayFileSlotNumberCode(retVal);
		return retVal;
	}

	private String parseOutBlackColorCode(String inputString) {
		return inputString.replace(blackColorHexCode, BLACK_COLOR_CODE);
	}

	private String parseOutRedColorCode(String inputString) {
		return inputString.replace(redColorHexCode, RED_COLOR_CODE);
	}

	private String parseOutGreenColorCode(String inputString) {
		return inputString.replace(greenColorHexCode, GREEN_COLOR_CODE);
	}

	private String parseOutBlueColorCode(String inputString) {
		return inputString.replace(blueColorHexCode, BLUE_COLOR_CODE);
	}

	private String parseOutVerySlowCode(String inputString) {
		return inputString.replace(verySlowHexCode, VERY_SLOW_CODE);
	}

	private String parseOutSlowCode(String inputString) {
		return inputString.replace(slowHexCode, SLOW_CODE);
	}
	
	private String parseOutPauseCode(String inputString) {
		return inputString.replace(pauseHexCode, PAUSE_CODE);
	}

	private String parseOutFastCode(String inputString) {
		return inputString.replace(fastHexCode, FAST_CODE);
	}

	private String parseOutFileSlotNumberCode(String inputString) {
		return inputString.replace(fileSlotHexCode, FILE_SLOT_CODE);
	}

	private String parseOutNameCode(String inputString) {
		return inputString.replace(nameHexCode, NAME_CODE);
	}

	private String parseInBlackColorCode(String inputString) {
		return inputString.replace(BLACK_COLOR_CODE, blackColorHexCode);
	}

	private String parseInRedColorCode(String inputString) {
		return inputString.replace(RED_COLOR_CODE, redColorHexCode);
	}

	private String parseInGreenColorCode(String inputString) {
		return inputString.replace(GREEN_COLOR_CODE, greenColorHexCode);
	}

	private String parseInBlueColorCode(String inputString) {
		return inputString.replace(BLUE_COLOR_CODE, blueColorHexCode);
	}

	private String parseInVerySlowCode(String inputString) {
		return inputString.replace(VERY_SLOW_CODE, verySlowHexCode);
	}

	private String parseInSlowCode(String inputString) {
		return inputString.replace(SLOW_CODE, slowHexCode);
	}
	
	private String parseInPauseCode(String inputString) {
		return inputString.replace(PAUSE_CODE, pauseHexCode);
	}

	private String parseInFastCode(String inputString) {
		return inputString.replace(FAST_CODE, fastHexCode);
	}

	private String parseInFileSlotNumberCode(String inputString) {
		return inputString.replace(FILE_SLOT_CODE, fileSlotHexCode);
	}

	private String parseInNameCode(String inputString) {
		return inputString.replace(NAME_CODE, nameHexCode);
	}

	private String parseAwayBlackColorCode(String inputString) {
		return inputString.replace(BLACK_COLOR_CODE, "");
	}

	private String parseAwayRedColorCode(String inputString) {
		return inputString.replace(RED_COLOR_CODE, "");
	}

	private String parseAwayGreenColorCode(String inputString) {
		return inputString.replace(GREEN_COLOR_CODE, "");
	}

	private String parseAwayBlueColorCode(String inputString) {
		return inputString.replace(BLUE_COLOR_CODE, "");
	}

	private String parseAwayVerySlowCode(String inputString) {
		return inputString.replace(VERY_SLOW_CODE, "");
	}

	private String parseAwaySlowCode(String inputString) {
		return inputString.replace(SLOW_CODE, "");
	}
	
	private String parseAwayPauseCode(String inputString) {
		return inputString.replace(PAUSE_CODE, "");
	}

	private String parseAwayFastCode(String inputString) {
		return inputString.replace(FAST_CODE, "");
	}

	private String parseAwayFileSlotNumberCode(String inputString) {
		return inputString.replace(FILE_SLOT_CODE, "");
	}

	private String parseAwayNameCode(String inputString) {
		return inputString.replace(NAME_CODE, "");
	}
	
	private String parseOutHoverCodes(String inputString) {
		char[] hoverCodeSizes = new char[12];
		hoverCodeSizes[0] = (char) Integer.parseInt("FF0A", 16);
		hoverCodeSizes[1] = (char) Integer.parseInt("FF0C", 16);
		hoverCodeSizes[2] = (char) Integer.parseInt("FF0E", 16);
		hoverCodeSizes[3] = (char) Integer.parseInt("FF10", 16);
		hoverCodeSizes[4] = (char) Integer.parseInt("FF12", 16);
		hoverCodeSizes[5] = (char) Integer.parseInt("FF14", 16);
		hoverCodeSizes[6] = (char) Integer.parseInt("FF16", 16);
		hoverCodeSizes[7] = (char) Integer.parseInt("FF18", 16);
		hoverCodeSizes[8] = (char) Integer.parseInt("FF1A", 16);
		hoverCodeSizes[9] = (char) Integer.parseInt("FF1C", 16);
		hoverCodeSizes[10] = (char) Integer.parseInt("FF1E", 16);
		hoverCodeSizes[11] = (char) Integer.parseInt("FF20", 16);

		char[] charHoverCode = new char[3];
		charHoverCode[0] = (char) Integer.parseInt("001A", 16);
		charHoverCode[2] = (char) Integer.parseInt("0002", 16);

		for (int i = 0; i < hoverCodeSizes.length; i++) {
			charHoverCode[1] = hoverCodeSizes[i];
			String charHoverString = new String(charHoverCode);
			while (inputString.contains(charHoverString)) {

				int charHoverIndex = inputString.indexOf(charHoverString);
				System.out.println("Parsed out " + (i + 1)
						+ " character hover code at index " + charHoverIndex);
				String tempStringOne = inputString.substring(0, charHoverIndex);
				String tempStringTwo = inputString.substring(charHoverIndex + 5
						+ i);
				inputString = tempStringOne + tempStringTwo;
			}
		}

		return inputString;
	}

	/*
	 * public String parseOutColorCodes(String inputString) { String[]
	 * colorCodes = { BLACK_COLOR_CODE, RED_COLOR_CODE, GREEN_COLOR_CODE,
	 * BLUE_COLOR_CODE };
	 * 
	 * char[] colorCodeChars = new char[4]; colorCodeChars[0] = (char)
	 * Integer.parseInt("0000", 16); colorCodeChars[1] = (char)
	 * Integer.parseInt("0001", 16); colorCodeChars[2] = (char)
	 * Integer.parseInt("0002", 16); colorCodeChars[3] = (char)
	 * Integer.parseInt("0003", 16);
	 * 
	 * char[] charColorCode = new char[4]; charColorCode[0] = (char)
	 * Integer.parseInt("001A", 16); charColorCode[1] = (char)
	 * Integer.parseInt("FF08", 16); charColorCode[2] = (char)
	 * Integer.parseInt("0000", 16);
	 * 
	 * for (int i = 0; i < colorCodes.length; i++) { charColorCode[3] =
	 * colorCodeChars[i]; String colorCodeString = new String(charColorCode);
	 * while (inputString.contains(colorCodeString)) {
	 * 
	 * int charHoverIndex = inputString.indexOf(colorCodeString);
	 * System.out.println("Parsed out " + colorCodes[i] + " code at index " +
	 * charHoverIndex); String tempStringOne = inputString.substring(0,
	 * charHoverIndex); String tempStringTwo = inputString
	 * .substring(charHoverIndex + 4); inputString = tempStringOne +
	 * colorCodes[i] + tempStringTwo; } }
	 * 
	 * return inputString; }
	 */

}
