package com.darkcube.bmgeditor.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.darkcube.bmgeditor.forms.BmgEditorMainForm;
import com.darkcube.bmgeditor.forms.PointerViewDialog;
import com.darkcube.bmgeditor.util.UIUtils;

public class PointerViewAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private BmgEditorMainForm form = null;

	public PointerViewAction(BmgEditorMainForm form) {
		putValue(NAME, "View Pointers...");
		putValue(SHORT_DESCRIPTION, "Views the pointers in this file");
		this.form = form;
	}

	public void actionPerformed(ActionEvent e) {
		PointerViewDialog pointerDialog = new PointerViewDialog(form, false, form.getBmgFile());
		UIUtils.centerOnScreen(pointerDialog);
		pointerDialog.setVisible(true);
	}

}