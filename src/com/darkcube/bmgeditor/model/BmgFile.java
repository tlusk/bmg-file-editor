package com.darkcube.bmgeditor.model;

import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import com.darkcube.bmgeditor.util.HexConverter;

public class BmgFile {

	@SuppressWarnings("unused")
	private byte[] originalBytes;

	private String originalText;

	private String header;

	private String inf1Section;

	private String inf1Header;

	private String dat1Header;

	private String flw1Section = "";

	private String fli1Section = "";

	private List<Pointer> pointerList;

	private int bmgSize;

	private int numPointers;

	private int inf1Address;

	private int inf1Size;

	private int dat1Address;

	private int dat1Size;

	private int flw1Address;

	private int flw1Size;

	private int fli1Address;

	private int fli1Size;

	public BmgFile(String originalText, byte[] originalBytes) {
		// Set the original text
		this.originalText = originalText;

		// Set the original bytes
		this.originalBytes = originalBytes;

		// Get the size of the file
		bmgSize = originalText.length() * 2;

		// Get the INF1 Address and Size
		inf1Address = Integer.parseInt("0020", 16);
		inf1Size = originalText.charAt((inf1Address / 2) + 2);

		// Get the DAT1 Address and Size
		dat1Address = inf1Address + inf1Size;
		//dat1Size = originalText.charAt((dat1Address / 2) + 2);
		//if (dat1Size == 65533 || dat1Size == 0) {
			byte[] dat1SizeBytes = new byte[4];
			dat1SizeBytes[0] = 0;
			dat1SizeBytes[1] = 0;
			dat1SizeBytes[2] = originalBytes[dat1Address + 5];
			dat1SizeBytes[3] = originalBytes[dat1Address + 4];
			dat1Size = HexConverter.byteArrayToInt(dat1SizeBytes);
		//}

		// Get the FLW1 Address and Size
		flw1Address = dat1Address + dat1Size;
		if (flw1Address + 2 <= bmgSize) {
			flw1Size = originalText.charAt((flw1Address / 2) + 2);

			// Get the FLI1 Address and Size
			fli1Address = flw1Address + flw1Size;
			if (fli1Address + 2 <= bmgSize) {
				fli1Size = originalText.charAt((fli1Address / 2) + 2);
			} else {
				fli1Address = -1;
				fli1Size = -1;
			}
		} else {
			flw1Address = -1;
			flw1Size = -1;
			fli1Address = -1;
			fli1Size = -1;
		}

		// Get number of pointers
		numPointers = originalText.charAt((inf1Address / 2) + 4);

		// Assert that the two ways to get the FLW1 Address are equal
		int flw1Address2 = originalText.charAt(4);
		if (flw1Address2 == 65533) {
			byte[] flw1Address2Bytes = new byte[4];
			flw1Address2Bytes[0] = 0;
			flw1Address2Bytes[1] = 0;
			flw1Address2Bytes[2] = originalBytes[9];
			flw1Address2Bytes[3] = originalBytes[8];
			flw1Address2 = HexConverter.byteArrayToInt(flw1Address2Bytes);
		}
		if (flw1Address2 != flw1Address && !(flw1Address == -1 && flw1Address2 == bmgSize)) {
			System.err.println("ERROR! FLW1 Addresses Don't Match!");
			System.err.println("Actual FLW1 Address: " + flw1Address);
			System.err.println("Header FLW1 Address: " + flw1Address2);
		}

		// Get the pointers
		String pointerText = originalText.substring(((inf1Address / 2) + 8), (inf1Address + inf1Size) / 2);
		pointerList = new LinkedList<Pointer>();
		for (int i = 0; i < numPointers; i++) {
			int pointerAddress = pointerText.charAt(i * 4);
			//if (pointerAddress == 65533) {
			byte[] pointerAddressBytes = new byte[4];
			pointerAddressBytes[0] = 0;
			pointerAddressBytes[1] = 0;
			pointerAddressBytes[2] = originalBytes[49 + (i * 8)];
			pointerAddressBytes[3] = originalBytes[48 + (i * 8)];
			pointerAddress = HexConverter.byteArrayToInt(pointerAddressBytes);
			//}
			if (i > 280) {
				System.out.println("TEST");
			}
			int pointerRows = pointerText.charAt((i * 4) + 3);
			pointerList.add(new Pointer(pointerAddress, pointerRows));
		}

		for (int i = 0; i < numPointers; i++) {
			pointerList.get(i).setPointerText(getPointerNumber(i));
		}

		// Get the text sections and fill them
		header = originalText.substring(0, inf1Address / 2);
		inf1Section = originalText.substring(inf1Address / 2, (inf1Address + inf1Size) / 2);
		inf1Header = inf1Section.substring(0, 8);
		dat1Header = originalText.substring(dat1Address / 2, ((dat1Address + pointerList.get(0).getAddress()) / 2) + 4);
		if (flw1Address != -1) {
			flw1Section = originalText.substring(flw1Address / 2, (flw1Address + flw1Size) / 2);
		}
		if (fli1Address != -1) {
			if (((fli1Address + fli1Size) / 2) > bmgSize / 2) {
				fli1Section = originalText.substring(fli1Address / 2, bmgSize / 2);
			} else {
				fli1Section = originalText.substring(fli1Address / 2, (fli1Address + fli1Size) / 2);
			}
		}
	}

	public Pointer getPointer(int numPointer) {
		return pointerList.get(numPointer);
	}

	public void setPointer(int numPointer, Pointer pointer) {
		pointerList.set(numPointer, pointer);
	}

	public String getPointerText(int numPointer) {
		String retVal = pointerList.get(numPointer).getPointerText();
		return retVal;
	}

	public void setPointerText(int numPointer, String text) {
		int newPointerSize = text.length();
		int oldPointerSize = pointerList.get(numPointer).length();

		int sizeDifference = newPointerSize - oldPointerSize;

		for (int i = numPointer + 1; i < numPointers; i++) {
			pointerList.get(i).changeAddress(sizeDifference * 2);
		}
		pointerList.get(numPointer).setPointerText(text);
		dat1Size += sizeDifference * 2;
	}

	private String getPointerNumber(int num) {
		String retVal = null;

		if (num < pointerList.size() - 1) {
			Pointer pointer1 = pointerList.get(num);
			Pointer pointer2 = pointerList.get(num + 1);

			retVal = originalText.substring(((dat1Address + pointer1.getAddress()) / 2) + 4, ((dat1Address + pointer2.getAddress()) / 2) + 4);
		} else if (num == pointerList.size() - 1) {
			Pointer pointer1 = pointerList.get(num);
			int pointerEnd = (dat1Address + dat1Size) / 2;
			
			retVal = originalText.substring(((dat1Address + pointer1.getAddress()) / 2) + 4, pointerEnd);
		}

		return retVal;

	}

	public String toString() {
		String dataString = getConstructedInf1Block();
		dataString += getConstructedDat1Block();
		reCalculateflAddresses();
		dataString += flw1Section;
		dataString += fli1Section;

		char[] headerChars = header.toCharArray();
		headerChars[4] = (char) flw1Address;
		header = new String(headerChars);

		String retVal = header + dataString;

		bmgSize = retVal.length() * 2;
		return retVal;
	}

	public byte[] getBytes() {
		String string = toString();
		byte[] bytes = null;
		try {
			bytes = string.getBytes("UTF-16LE");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (bytes[8] == -3 && bytes[9] == -1) {
			byte[] flw1AddressBytes = HexConverter.intToDWord(flw1Address);
			bytes[8] = flw1AddressBytes[0];
			bytes[9] = flw1AddressBytes[1];
			System.out.println("TOO BIG ERROR!!!");
		}
		for (int i = 0; i < numPointers * 8; i += 8) {
			System.out.println(bytes[48 + i]);
			System.out.println(bytes[49 + i]);
			if (bytes[48 + i] == -3 && bytes[49 + i] == -1) {
				Pointer pointer = pointerList.get(i / 8);
				int pointerAddress = pointer.getAddress();
				byte[] pointerAddressBytes = HexConverter.intToDWord(pointerAddress);
				bytes[48 + i] = pointerAddressBytes[0];
				bytes[49 + i] = pointerAddressBytes[1];
				System.out.println("TOO BIG POINTER!!!");
			}
		}
		if (bytes[dat1Address + 4] == -3 && bytes[dat1Address + 5] == -1) {
			System.out.println("DAT1 TOO BIG ERROR!");
			byte[] dat1SizeBytes = HexConverter.intToDWord(dat1Size);
			bytes[dat1Address + 4] = dat1SizeBytes[0];
			bytes[dat1Address + 5] = dat1SizeBytes[1];
		} else {
			System.out.println(dat1Address);
			System.out.println(bytes[dat1Address + 4]);
			System.out.println(bytes[dat1Address + 5]);
		}
		return bytes;
	}

	public void reCalculateflAddresses() {
		if (flw1Address != -1) {
			flw1Address = dat1Address + dat1Size;
			if (fli1Address != -1) {
				fli1Address = flw1Address + flw1Size;
			}
		}
	}

	public String getConstructedInf1Block() {
		String retVal = inf1Header;
		for (int i = 0; i < numPointers; i++) {
			retVal += pointerList.get(i).getHeaderString();
		}
		while (retVal.length() < (inf1Size / 2)) {
			retVal += (char) Integer.parseInt("0000", 16);
		}
		return retVal;
	}

	public String getConstructedDat1Block() {
		int numCharstoAdd = dat1Size % 16;
		System.out.println(dat1Size);
		numCharstoAdd = 16 - numCharstoAdd;

		if (numCharstoAdd == 16) {
			numCharstoAdd = 0;
		}

		dat1Size += numCharstoAdd;

		String headerPartOne = dat1Header.substring(0, 2);
		String headerPartTwo = dat1Header.substring(3);
		String retVal = headerPartOne;
		retVal += (char) dat1Size;
		retVal += headerPartTwo;
		for (int i = 0; i < numPointers; i++) {
			retVal += pointerList.get(i).getPointerText();
		}
		for (int i = 0; i < numCharstoAdd / 2; i++) {
			retVal += (char) Integer.parseInt("0000", 16);
		}
		return retVal;
	}

	public int getBmgSize() {
		return bmgSize;
	}

	public void setBmgSize(int bmgSize) {
		this.bmgSize = bmgSize;
	}

	public int getDat1Address() {
		return dat1Address;
	}

	public void setDat1Address(int dat1Address) {
		this.dat1Address = dat1Address;
	}

	public int getDat1Size() {
		return dat1Size;
	}

	public void setDat1Size(int dat1Size) {
		this.dat1Size = dat1Size;
	}

	public int getFli1Address() {
		return fli1Address;
	}

	public void setFli1Address(int fli1Address) {
		this.fli1Address = fli1Address;
	}

	public int getFli1Size() {
		return fli1Size;
	}

	public void setFli1Size(int fli1Size) {
		this.fli1Size = fli1Size;
	}

	public int getFlw1Address() {
		return flw1Address;
	}

	public void setFlw1Address(int flw1Address) {
		this.flw1Address = flw1Address;
	}

	public int getFlw1Size() {
		return flw1Size;
	}

	public void setFlw1Size(int flw1Size) {
		this.flw1Size = flw1Size;
	}

	public int getInf1Address() {
		return inf1Address;
	}

	public void setInf1Address(int inf1Address) {
		this.inf1Address = inf1Address;
	}

	public int getInf1Size() {
		return inf1Size;
	}

	public void setInf1Size(int inf1Size) {
		this.inf1Size = inf1Size;
	}

	public String getOriginalText() {
		return originalText;
	}

	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}

	public int getNumPointers() {
		return numPointers;
	}

	public void setNumPointers(int numPointers) {
		this.numPointers = numPointers;
	}

	public List<Pointer> getPointerList() {
		return pointerList;
	}

	public void setPointerList(List<Pointer> pointerList) {
		this.pointerList = pointerList;
	}

	public String getFli1Section() {
		return fli1Section;
	}

	public void setFli1Section(String fli1Section) {
		this.fli1Section = fli1Section;
	}

	public String getFlw1Section() {
		return flw1Section;
	}

	public void setFlw1Section(String flw1Section) {
		this.flw1Section = flw1Section;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getInf1Section() {
		return inf1Section;
	}

	public void setInf1Section(String inf1Section) {
		this.inf1Section = inf1Section;
	}
}
