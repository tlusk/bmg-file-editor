package com.darkcube.bmgeditor.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileInput {

	public static byte[] getFileBytes(File file) throws IOException {
		int fileSize = (int) file.length();
		byte[] retVal = new byte[fileSize];
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
		while (in.read(retVal) != -1) {
		}
		in.close();
		return retVal;
	}

}
