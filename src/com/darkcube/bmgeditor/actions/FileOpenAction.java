package com.darkcube.bmgeditor.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import com.darkcube.bmgeditor.forms.BmgEditorMainForm;
import com.darkcube.bmgeditor.util.FileInput;

public class FileOpenAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private BmgEditorMainForm form = null;

	public FileOpenAction(BmgEditorMainForm form) {
		putValue(NAME, "Open...");
		putValue(SHORT_DESCRIPTION, "Opens a text document");
		this.form = form;
	}

	public void actionPerformed(ActionEvent e) {
		File dirToOpen = new File("F:/Zelda/Message");
		if (!dirToOpen.exists()) {
			dirToOpen = new File("C:/Documents and Settings/Administrator/Desktop/dslazy/NDS_UNPACK/data/Japanese/Message");
		}

		FileFilter bmgFileFilter = new FileFilter() {
			public boolean accept(File file) {
				return file.getAbsolutePath().endsWith(".bmg") || file.isDirectory();
			}

			public String getDescription() {
				return "BMG Files";
			}
		};

		JFileChooser fileChooser = new JFileChooser(dirToOpen);
		fileChooser.setFileFilter(bmgFileFilter);

		int returnVal = fileChooser.showOpenDialog(form);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("You chose to open this file: " + fileChooser.getSelectedFile().getName());
			try {
				byte[] bytes = FileInput.getFileBytes(fileChooser.getSelectedFile());
				String text = new String(bytes, "UTF-16LE");
				form.setAllFields(text, bytes);
				form.setTitle("Zelda BMG Editor " + fileChooser.getSelectedFile().getName());
				form.setOpenedFile(fileChooser.getSelectedFile());
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e2) {
				e2.printStackTrace();
			} catch (IOException e3) {
				e3.printStackTrace();
			}

		}
	}

}