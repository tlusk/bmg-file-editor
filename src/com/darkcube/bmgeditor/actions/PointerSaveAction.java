package com.darkcube.bmgeditor.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import com.darkcube.bmgeditor.forms.BmgEditorMainForm;
import com.darkcube.bmgeditor.util.FileOutput;

public class PointerSaveAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private BmgEditorMainForm form = null;

	public PointerSaveAction(BmgEditorMainForm form) {
		putValue(NAME, "Save Pointer...");
		putValue(SHORT_DESCRIPTION, "Saves the currently opened pointer");
		this.form = form;
	}

	public void actionPerformed(ActionEvent e) {
		File dirToOpen = new File("F:/Zelda");
		JFileChooser fileChooser = new JFileChooser(dirToOpen);
		int returnVal = fileChooser.showSaveDialog(form);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("You chose to save this file: " + fileChooser.getSelectedFile().getName());
			try {
				FileOutput.saveBytes(fileChooser.getSelectedFile(), form.getFileText().getBytes("UTF-16LE"));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e2) {
				e2.printStackTrace();
			} catch (IOException e3) {
				e3.printStackTrace();
			}

		}
	}

}