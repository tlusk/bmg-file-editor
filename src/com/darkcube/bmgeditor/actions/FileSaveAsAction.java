package com.darkcube.bmgeditor.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import com.darkcube.bmgeditor.forms.BmgEditorMainForm;
import com.darkcube.bmgeditor.util.FileInput;
import com.darkcube.bmgeditor.util.FileOutput;

public class FileSaveAsAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private BmgEditorMainForm form = null;

	public FileSaveAsAction(BmgEditorMainForm form) {
		putValue(NAME, "Save As...");
		putValue(SHORT_DESCRIPTION, "Saves the bmg file to another file");
		this.form = form;
	}

	public void actionPerformed(ActionEvent e) {
		File dirToOpen = new File("F:/Zelda/Message");
		if (!dirToOpen.exists()) {
			dirToOpen = new File("C:/Documents and Settings/Administrator/Desktop/dslazy/NDS_UNPACK/data/Japanese/Message");
		}
		JFileChooser fileChooser = new JFileChooser(dirToOpen);
		int returnVal = fileChooser.showSaveDialog(form);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("You chose to save this file: " + fileChooser.getSelectedFile().getName());
			try {
				FileOutput.saveBytes(fileChooser.getSelectedFile(), form.getBmgFile().getBytes());
				byte[] savedFile = FileInput.getFileBytes(fileChooser.getSelectedFile());
				String text = new String(savedFile, "UTF-16LE");
				form.setTitle("Zelda BMG Editor " + fileChooser.getSelectedFile().getName());
				form.setOpenedFile(fileChooser.getSelectedFile());
				form.setAllFields(text, savedFile);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e2) {
				e2.printStackTrace();
			} catch (IOException e3) {
				e3.printStackTrace();
			}

		}
	}

}