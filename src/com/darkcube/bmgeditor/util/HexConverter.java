package com.darkcube.bmgeditor.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class HexConverter {

	public static String toHex(String str) {
		String retVal = null;
		try {
			byte[] bytes = str.getBytes("UTF-16");
			for (int i = 0; i < bytes.length; i++) {
				if (retVal == null) {
					retVal = byteToHex(bytes[i]);
				} else {
					retVal += byteToHex(bytes[i]);
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public static String byteToHex(byte b) {
		// Returns hex String representation of byte b
		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
		return new String(array).toUpperCase();
	}

	public static String charToHex(char c) {
		// Returns hex String representation of char c
		byte hi = (byte) (c >>> 8);
		byte lo = (byte) (c & 0xff);
		return byteToHex(hi) + byteToHex(lo);
	}

	// from evnafets' reply
	public static byte[] intToByteArray(final int integer) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);
		try {
			dos.writeInt(integer);
			dos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bos.toByteArray();
	}

	// My code for inversed process.
	public static int byteArrayToInt(final byte[] byteArray) {
		ByteArrayInputStream bos = new ByteArrayInputStream(byteArray);
		DataInputStream dos = new DataInputStream(bos);
		int retVal = 0;
		try {
			retVal = dos.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return retVal;
	}

	// returns a byte array of length 4
	public static byte[] intToDWord(int i) {
		byte[] dword = new byte[4];
		dword[0] = (byte) (i & 0x00FF);
		dword[1] = (byte) ((i >> 8) & 0x000000FF);
		dword[2] = (byte) ((i >> 16) & 0x000000FF);
		dword[3] = (byte) ((i >> 24) & 0x000000FF);
		return dword;
	}

	public static char[] intToChar(int integer) {

		// temp array of char.
		char[] charArray = new char[4];

		// do right shift to the bytes of the integer to reach the byte 
		// which we want then do ( and logic ) to delet 
		// the other values then save it in on byte.
		charArray[0] = (char) (byte) ((integer >> 24) & 0x000000FF);
		charArray[1] = (char) (byte) ((integer >> 16) & 0x0000FF);
		charArray[2] = (char) (byte) ((integer >> 8) & 0x00FF);
		charArray[3] = (char) (byte) (integer & 0xFF);

		return charArray;
	}

	public static int charToInt(char[] charInteger) {
		// Temp integer                   
		int integer = 0;

		// do left shift to every bytes in the array with it location. 
		// then do ( and logic ) to delete the other values. 
		// then add it to the temp integer.                      
		integer += (int) ((((byte) charInteger[0]) << 24) & 0xFF000000);
		integer += (int) ((((byte) charInteger[1]) << 16) & 0xFF0000);
		integer += (int) ((((byte) charInteger[2]) << 8) & 0xFF00);
		integer += (int) (((byte) charInteger[3]) & 0xFF);

		return integer;
	}

}
