package com.darkcube.bmgeditor.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.AbstractAction;

import com.darkcube.bmgeditor.forms.BmgEditorMainForm;
import com.darkcube.bmgeditor.util.FileInput;
import com.darkcube.bmgeditor.util.FileOutput;

public class FileSaveAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private BmgEditorMainForm form = null;

	public FileSaveAction(BmgEditorMainForm form) {
		putValue(NAME, "Save");
		putValue(SHORT_DESCRIPTION, "Saves the bmg file");
		this.form = form;
	}

	public void actionPerformed(ActionEvent e) {
		File fileToSave = form.getOpenedFile();
		try {
			FileOutput.saveBytes(fileToSave, form.getBmgFile().getBytes());
			byte[] savedFile = FileInput.getFileBytes(fileToSave);
			form.setTitle("Zelda BMG Editor " + fileToSave.getName());
			String text = new String(savedFile, "UTF-16LE");
			form.setAllFields(text, savedFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		} catch (IOException e3) {
			e3.printStackTrace();
		}
	}

}