package com.darkcube.bmgeditor.app;

import com.darkcube.bmgeditor.forms.BmgEditorMainForm;
import com.darkcube.bmgeditor.util.UIUtils;

public class BmgEditorApplication {

	public static void main(String[] args) {
		BmgEditorMainForm mainForm = new BmgEditorMainForm();
		UIUtils.centerOnScreen(mainForm);
		mainForm.setVisible(true);
	}
}
