package com.darkcube.bmgeditor.util;

import java.awt.AWTError;
import java.awt.Dimension;
import java.awt.Toolkit;

public class UIUtils {

	public static void centerOnScreen(java.awt.Component comp) {
		Dimension screenSize;
		try {
			screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		} catch (AWTError awe) {
			screenSize = new Dimension(640, 480);
		}
		Dimension frameSize = comp.getSize(); //Fill screen if the screen is smaller that qtfSize.
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		int x = (screenSize.width / 2) - (frameSize.width / 2);
		int y = (screenSize.height / 2) - (frameSize.height / 2);
		comp.setLocation(x, y); // Center the screen
	}

}
